package com.apuntesdejava.payaramicro.programmatically;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

public class EmbeddedPayara01 {

    public static void main(String[] args) throws BootstrapException {
        PayaraMicro.bootstrap();//ejecuta PayaraMicro inmediatamente
    }
}
