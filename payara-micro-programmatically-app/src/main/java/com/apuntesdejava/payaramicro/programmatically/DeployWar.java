/*
 * Copyright (C) 2017 diego
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.apuntesdejava.payaramicro.programmatically;

import fish.payara.micro.BootstrapException;
import fish.payara.micro.PayaraMicro;

/**
 *
 * @author diego
 */
public class DeployWar {

    public static void main(String[] args) throws BootstrapException {
        PayaraMicro instance = PayaraMicro.getInstance();
        instance.setHttpPort(9090);
        instance.addDeployment("e:\\users\\diego\\documents\\NetBeansProjects\\blog\\payara-micro-demo\\rest-demo-services\\target\\rest-demo-services-1.0.war");
        instance.bootStrap();
        
        //instance.addDeployFromGAV("com.apuntesdejava,rest-demo-services,1.0").bootStrap();

    }
}
