package com.apuntesdejava.rest.demo.services;

import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author diego
 */
@Singleton
@Path("empleado")
public class EmpleadoREST {

    @Inject
    private EmpleadoFacade empleadoFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Empleado> findAll() {
        List<Empleado> empleados = empleadoFacade.findAll();
        return empleados;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Empleado empleado) {
        empleadoFacade.create(empleado);
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id) {
        Empleado entity = empleadoFacade.find(id);
        empleadoFacade.remove(entity);
    }
}
